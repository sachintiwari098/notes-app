import { Component, OnInit } from '@angular/core';

export class Notes {
  title: string;
  content: string;
  timeStamp: any;
  index: any;
}

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  contentTitle: string;
  contentModel: string;
  contentTimeStamp: any;
  contentIndex: any;
  search: string;
  notes: Notes[];
  searchNotes: Notes[];
  mode: string;
  backdrop: boolean;
  sideNavOpen: boolean;

  constructor() {
    this.contentTitle = '';
    this.contentModel = '';
    this.contentIndex = '0';
    this.contentTimeStamp = new Date().toLocaleString();

    const defaultnotes: Notes = {
      title: 'New Note',
      content: '',
      timeStamp: new Date().toLocaleString(),
      index: '0'
    };
    this.notes = [];
    if (localStorage.length) {
      for (let p = 0; p < localStorage.length; p++) {
        let localStorageObj = JSON.parse(localStorage.getItem(String(p)));
        const storedNote: Notes = {
          title: localStorageObj.title,
          content: localStorageObj.content,
          timeStamp: localStorageObj.timeStamp,
          index: p,
        };
        if (p == 0) {
          this.contentModel = localStorageObj.content;
          this.contentTimeStamp = localStorageObj.timeStamp
        }
        this.notes.push(storedNote);
      }
    } else {
      this.notes = [defaultnotes];
    }
    this.searchNotes = this.notes;
  }
  ngOnInit() {
    this.onResize();
  }

  addNote() {
    this.contentTitle = 'New Note';
    this.contentModel = '';
    this.contentTimeStamp = new Date().toLocaleString();
    this.contentIndex = String(this.notes.length);
    const newNotes: Notes = {
      title: this.contentTitle,
      content: this.contentModel,
      timeStamp: this.contentTimeStamp,
      index: this.contentIndex,
    };
    this.notes.push(newNotes);
    this.contentModel = '';
    this.contentTimeStamp = new Date().toLocaleString();
    this.searchNotes = this.notes;
  }

  showNote(note: any) {
    this.contentModel = this.notes[note.index].content;
    this.contentTimeStamp = this.notes[note.index].timeStamp;
    this.contentIndex = note.index;
    if (this.backdrop) {
      document.getElementById('sidenav-button').click();
    }
  }

  changeNote() {
    if (this.contentModel) {
      this.contentTitle = this.contentModel;
      this.notes[this.contentIndex].title = this.notes[this.contentIndex].content = this.contentTitle;
    } else {
      this.contentTitle = "New Note";
      this.notes[this.contentIndex].title = this.notes[this.contentIndex].content = this.contentTitle;
    }
    this.contentTimeStamp = new Date().toLocaleString();
    this.notes[this.contentIndex].timeStamp = this.contentTimeStamp;
    let localStorageObj = {
        "title" : this.contentTitle,
        "content" : this.contentModel,
        "timeStamp" : this.contentTimeStamp
    }
    localStorage.setItem(this.contentIndex, JSON.stringify(localStorageObj));
    this.searchNotes = this.notes;
  }

  deleteNote() {
    this.notes.splice(this.contentIndex, 1);
    localStorage.clear();
    if (this.notes.length == 0) {
      const defaultNotes: Notes = {
        title: 'New Note',
        content: '',
        timeStamp: new Date().toLocaleString(),
        index: '0'
      };
      this.notes.push(defaultNotes);
    }
    for (let p = 0; p < this.notes.length; p++) {
      this.notes[p].index = p;
      let localStorageObj = {
        "title" : this.notes[p].title,
        "content" : this.notes[p].content,
        "timeStamp" : this.notes[p].timeStamp
      }
      localStorage.setItem(String(p), JSON.stringify(localStorageObj));
      if (p == 0) {
        this.contentModel = this.notes[p].content;
        this.contentTimeStamp = this.notes[p].timeStamp;
        this.contentIndex = 0;
      }
    }
    this.searchNotes = this.notes;
  }

  searchNote(value) {
    this.searchNotes = this.notes.filter(
      (val) => val['title'].includes(value));
    for (let p in this.searchNotes) {

    }
    this.contentModel = this.searchNotes[0].content;
  }

  toggleSideNav(sidenav:any) {
    sidenav.toggle();
  }

  onResize() {
    if (window.innerWidth <= 600) {
      this.mode = "over";
      this.backdrop = true;
      this.sideNavOpen = false;
    } else {
      this.mode = "side";
      this.backdrop = false;
      this.sideNavOpen = true;
    }
  }

}
